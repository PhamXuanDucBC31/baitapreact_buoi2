import logo from "./logo.svg";
import "./App.css";
import Ex_BaiTapThuKinh from "./Ex_BaiTapThuKinh/Ex_BaiTapThuKinh";

function App() {
  return (
    <div className="App">
      <Ex_BaiTapThuKinh />
    </div>
  );
}

export default App;
