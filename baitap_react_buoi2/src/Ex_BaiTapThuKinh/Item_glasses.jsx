import React, { Component } from "react";
import { glassesArr } from "./Data_glasses";

export default class Item_glasses extends Component {
  render() {
    return (
      <img className="w-75 h-100 pb-5" src={this.props.glasses.url} alt="" />
    );
  }
}
