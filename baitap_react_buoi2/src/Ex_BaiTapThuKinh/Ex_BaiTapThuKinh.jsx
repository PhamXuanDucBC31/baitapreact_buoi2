import React, { Component } from "react";
import { glassesArr } from "./Data_glasses";
import Item_glasses from "./Item_glasses";
import style from "./Style.css";

export default class Ex_BaiTapThuKinh extends Component {
  state = {
    glassList: glassesArr,
    imgSrc: "",
    glassesName: "",
    glassesPrice: "",
    glassesDesc: "",
  };

  renderGlassesModel = (id) => {
    this.setState({
      imgSrc: `./img/v${id}.png`,
      glassesName: `${glassesArr[id - 1].name}`,
      glassesPrice: `Price: ${glassesArr[id - 1].price} $`,
      glassesDesc: `${glassesArr[id - 1].desc}`,
    });
  };

  renderGlasses = () => {
    let Glasses = this.state.glassList.map((item) => {
      return (
        <div
          onClick={() => {
            this.renderGlassesModel(item.id);
          }}
          className="col-4"
        >
          <Item_glasses glasses={item} key={item.id} />
        </div>
      );
    });
    return Glasses;
  };

  render() {
    return (
      <>
        <p className="title">BÀI TẬP THỬ KÍNH</p>
        <div className="container d-flex">
          <div className="img_container w-25">
            <img src="./img/model.jpg" alt="" />

            <div className="glassesModel">
              <img src={this.state.imgSrc} alt="" />
            </div>

            <div className="glassesInfo">
              <p className="text-warning">{this.state.glassesName}</p>
              <span className="text-success">{this.state.glassesPrice}</span>
              <h5 className="text-white">{this.state.glassesDesc}</h5>
            </div>
          </div>

          <div className="w-75 row">{this.renderGlasses()}</div>
        </div>
      </>
    );
  }
}
